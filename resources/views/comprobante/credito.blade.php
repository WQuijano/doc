<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title></title>
	<link rel="stylesheet" type="text/css" href="./bootstrap.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

	
</head>
<body>

	<!--- Inicio de factura  -->
	
	<div class="container" >
		<form id="from" method="post" action="">
		<div class="3"></div>
		<div class="col-md-12 "  style="padding: 15px;box-shadow: 0px 0px 3px 3px black;margin-top: 10%; border-color: black; border-top: 10%; ">
			<div class="row">
				<div class="col-md-7"><p style="font-weight: 700">NOMBRE,DENOMINACION O RAZON<br>SOCIAL DEL CONTRIBUYENTE EMISOR:        <span id="Nombre" style="font-weight: 700;"></span></p>
					<p style="font-weight: 700">GIRO/ACTIVIDAD:  <span id="Giro" style="font-weight: 700;"></span></p>
					<p style="font-weight: 700">DIRECCION: <span id="Direccion" style="font-weight: 700;"></span></p>
					<p style="font-weight: 700">FECHA:<input type="text" name="" class="col-md-7" readonly="readonly" value="<?php echo date('Y-m-d') ?>" style="border: white" ></p>
				</div>
				<div class="col-md-4" style="box-shadow: 2px 3px 5px 3px  black ; width: 1%; height: 1%; margin-top: 10px; ">
					<p style="font-weight: 700">COMPROBANTE DE CREDITO FISCAL</p>
					<P style="font-weight: 700">N°:<input type="text" name="" id="n" class="col-md-7" style="border: white"></P>
					<p style="font-weight: 700">N.I.T.:<input type="text" name="" id="nit2" class="col-md-7" style="border: white"></p>
					<P style="font-weight: 700">N.R.C.:<input type="text" name="" id="nrc2" class="col-md-7" style="border: white"></P>
				</div>

			</div>
			<div class="container-fluid " style="box-shadow: 0px 0px 3px 3px black; border-color: black; border-top: 10%; ">

				<div class="col-md-12">
					<p style="font-weight: 700">NOMBRE,DENOMINACION O RAZON SOCIAL DEL CONTRIBUYENTE EMISOR:<input type="text" id="Nombre2" class="col-md-12" style="border: white;" placeholder=" Empresa S.A. de C.V." maxlength="60"></p>
				</div>
				<div class="row" style="box-shadow: 0px 0px 2px 2px black; border-color: black; border-top: 10%; ">
					<div class="col-md-12" style="box-shadow: 0px 0px 2px 2px black"><p style="font-weight: 700">DIRECCION:<input type="text" maxlength="60" name="Direccion2" id="Direccion2" class="col-md-9" style="border: white" placeholder="Bulevard 503, calle 200,oficinas #600"></p></div>
					

				</div>
				<div class="row">
					<div class="col-md-4" style="box-shadow: 0px 0px 1px 1px black"><p style="font-weight: 700">N.I.T.:<input type="text" name="" id="nit" class="col-md-10" style="border: white;margin-top: 5px" maxlength="17"></p></div>
					<div class="col-md-3" style="box-shadow: 0px 0px 1px 1px black"><p style="font-weight: 700">N.R.C.:<input type="text" name="" id="nrc" class="col-md-9" maxlength="8" style="border: white;margin-top: 5px" placeholder="264575-5"></p></div>
					<div class="col-md-5" style="box-shadow: 0px 0px 1px 1px black"><p style="font-weight: 700">GIRO:<input type="text" name="" class="col-md-9" maxlength="60" id="Giro2" style="border: white; margin-top: 5px" placeholder="Mantenimiento de Computadoras"></p></div>
				</div>
				<br><br><br>
				<div class="row">
					<div   style="width: 900px;height: 29px;box-shadow: 0px 0px 1px 1px black"><p style="font-weight: 500;font-size: 20px">TOTAL</p> </div>

					<div   style=" width: 180px; height: 29px;box-shadow: 0px 0px 1px 1px black"><p style="font-weight: 500;font-size: 20px">US$<input type="number" name="" placeholder="10.00" style="border: white; margin-top: 3px; width: 140px;height: 20px"></p></div>


				</div>
			</div>

		</div>

		</form>
	</div>
	<script >
		$(document).ready(function(){
			$('#Nombre2').keyup(function(){
				var valor = $(this).val();
				$('#Nombre').text(valor);
			});

			$('#Giro2').keyup(function(){
				var valor = $(this).val();
				$('#Giro').text(valor);
			});

			$('#Direccion2').keyup(function(){
				var valor = $(this).val();
				$('#Direccion').text(valor);
			});

			$('#nit').mask("9999-999999-999-9", {placeholder : '9999-999999-999-9'});
			$('#nrc').mask("999999-9", {placeholder : '999999-9'});



			$('#n').mask("9999", {placeholder : '9999'});
			$('#nit2').mask("9999-999999-999-9", {placeholder : '9999-999999-999-9'});
			$('#nrc2').mask("999999-9", {placeholder : '999999-9'});
		});

		
	</script>

</body>
</html>
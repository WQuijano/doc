<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css" rel="stylesheet"/>

<div class="container" >
  <div class="row" >
  <form action="" method="GET" onsubmit="return validar();" style="font-size: 100%; box-shadow: 3px 4px 5px black; border-color: 10px black; margin-top: 10%;">
    <table id="tablaprueba"></table>
<div style="margin-top: 10%;">
    <input type="submit" name="Guardar" value="GUARDAR" type="button" class="btn btn-primary mr-2"> 
    <button type="button" class="btn btn-primary mr-2" onclick="agregarFila()">Agregar Fila</button>
      <button type="button" class="btn btn-danger" onclick="eliminarFila()">Eliminar Fila</button>
    </div>
  </form>
      
  </div>
</div>


<script type="text/javascript">
  function agregarFila(){
  document.getElementById("tablaprueba").insertRow(-1).innerHTML = '<input type="text" name="nombre" id="nombre" style="margin: 1px" class="form"><input type="text" name="apellido" id="apellido" class="form" style="margin: 1px" >';
}

function eliminarFila(){
  var table = document.getElementById("tablaprueba");
  var rowCount = table.rows.length;
  //console.log(rowCount);
  
  if(rowCount <= 1){
    alert('No se puede eliminar el encabezado');
  }else{
    table.deleteRow(rowCount -1);
  }
}

function validar(){
  var nombre = document.getElementById("nombre").value;
  var apellido = document.getElementById("apellido").value;

  if(nombre.length==0){
    document.getElementById("nombre").style.borderColor="red";
     document.getElementById("nombre").style.boxShadow="2px 4px 5px red";
    return false;
  }else{
    document.getElementById("nombre").style.borderColor="green";
     document.getElementById("nombre").style.boxShadow="2px 4px 5px green";
  }

   if(apellido.length==0){
    document.getElementById("apellido").style.borderColor="red";
    document.getElementById("apellido").style.boxShadow="2px 4px 5px red";
    return false;
  }else{
    document.getElementById("apellido").style.borderColor="green";
    document.getElementById("apellido").style.boxShadow="2px 4px 5px green";
  }
  return true;
}
</script>
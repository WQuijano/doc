<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class credito extends Model
{
    protected $table = 'credito';
    protected $primarikey = 'id';

    protected $fillable = [
        'id_cliente', 'numero_comprobante', 'NIT','NRC','total',
    ];
}
